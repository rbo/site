/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./components/**/*", "./pages/**/*", "./layouts/**/*"],
  theme: {
    extend: {
      fontFamily: {
        serif: ["VT323", "Sans"],
      },
      fontSize: {
        h1: "64px",
        h2: "36px",
        h3: "32px",
        h4: "24px",
        body: "18px",
        caption: "14px",
      },
      colors: {
        beige: {
          100: "#ddd8cc",
        },
        primary: {
          500: "#f03100",
        },
        secondary: {
          500: "#6dea46",
        },
      },
    },
  },
  plugins: [],
  prefix: "tw-",
};
