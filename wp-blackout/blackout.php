<?php
/*
Plugin Name: Radio Blackout
Plugin URI:  https://radioblackout.org
Description: 
Version:     1.0
Author:      Radio Blackout
License:     AGPL 3.0

Software Foundation, either version 3 of the license, or any later version.

WPBlackout is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with (Radio Backout). If not, see (https://www.gnu.org/licenses/agpl-3.0.html).
*/

defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );

require_once('rbo_show.php');
require_once('rbo_podcast.php');
require_once('rbo_palinsesto.php');
require_once('rbo_cli.php');

// require_once('rbo_event.php');