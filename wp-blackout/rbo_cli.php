<?php

if ( defined( 'WP_CLI' ) && WP_CLI ) {

    /**
     * uno dei modi in cui nel tempo sono state associate le trasmissioni ai podcast e' attraverso un wordpress post_meta chiamato '_podcast_show'
     * qui cerco se effettivamente per il post corrente c'e' ed e' associato ad una trasmissione esistente.
     */
    function rbo_get_term_from_meta () {
        $podcast_show_meta = get_post_meta(get_the_ID(), '_podcast_show');
        if (is_array($podcast_show_meta) && count($podcast_show_meta) > 0
            && is_array($podcast_show_meta[0]) && count($podcast_show_meta[0]) > 0) {
            // $show = get_post($podcast_show_meta[0][0], OBJECT, 'shows');
            $term = get_term($podcast_show_meta[0][0], 'podcastfilter');
            if ($term) {
                // $showTermID = wp_create_term($podcast_show_meta[0][0], 'podcastfilter');
                return $term['term_id'];
            }
        }
    }

    /**
     * uno degli altri modi per associare trasmissioni ai podcast e' attraverso la tassonomia podcastfilter
     */
    function rbo_get_term_from_podcastfilter () {
        // provo con i podcastfilter o i tag (sai mai..)
        $terms = wp_get_object_terms( get_the_ID(), ['podcastfilter']);
        if ($terms) {
            foreach($terms as $term) {
                // per tutti i terms trovati associati a questo post cerco uno show con lo stesso slug
                $show = get_page_by_path($term->slug, OBJECT, ['shows']);
                if ($show) {
                    return $term->term_id;
                }
            }
        }
        return null;
    }


    /**
     * come sopra ma ovviamente alcuni podcastfilter sono scritti male e non matchano con le trasmissioni
     */
    function rbo_get_term_from_malformed_podcastfilter () {
        $podcastfilterMap = [
            'riot-on-sunset' => 'riot-on-sunset-strip',
            'musick' => 'musick-to-play-in-the-dark',

            'kalakuta' => 'radio-kalakuta',
            'radiokalakuta' => 'radio-kalakuta',

            'radio-kebab' => 'radiokebab',

            'quel-che-resta-della-notte' => 'jene-nella-notte',

            'tutto-squat-il-giornale-malandrino' => 'il-giornale-malandrino',
            'radio-bizzarre' => 'radio-bizarre',

            'tigrevstigre' => 'tigre-vs-tigre',
            'sono-un-ribelle' => 'sono-un-ribelle-mamma',

            'notripforcats' => 'no-trip-for-cats',
            'pomeriggio105' => 'pomeriggio-105',

            'ricongiunzioni' => 'congiunzioni',
            'a-c-a-b-2' => 'acab',
            'a-c-a-b' => 'acab',
            'black-mamba' => 'blackmamba',
            '19-59' => '19e59',

            'radio-no-tav' => 'radionotav',
            'radio-notav' => 'radionotav',
            'sayonara' => 'sayonara-bangkok',
            'voci-antropocene' => 'voci-dallantropocene',
            'controbande' => 'contro-bande',
            'bastioniorione' => 'i-bastioni-di-orione',
            'ascuppuluni' => 'a-scuppuluni',
            'resetclub' => 'reset-club',
            'assadaka' => 'assadaka-amicizia',

            'bit' => 'il-bit-ce-o-non-ce',
            'tdm-scio' => 'teste-di-minchia-scio',
            'imballata' => 'imballata-w-bubble-wrap'
        ];

        $terms = wp_get_object_terms( get_the_ID(), ['podcastfilter']);
        foreach($terms as $term) {
            // ho trovato un podcastfilter malformato
            if (array_key_exists($term->slug, $podcastfilterMap)) {
                // cerco la trasmissione
                $show = get_page_by_path($podcastfilterMap[$term->slug], OBJECT, ['shows']);
                if ($show) {
                    $showTermID = wp_create_term($podcastfilterMap[$term->slug], 'podcastfilter');
                    return $showTermID['term_id'];
                }
            }
        }
        return null;
    }

    function rbo_get_term_from_title () {
        $title = get_the_title();
                
        $regexpMap = [
            ['/^radio kalakuta/i',  'radio-kalakuta'],
            ['/riot on sunset strip/i', 'riot-on-sunset-strip'],
            ['/^spot!/i', 'spot'],
            ['/^radio kebab/i', 'radiokebab'],
            ['/^overjoy/i', 'overjoy'],
            ['/^Musick To Play In The Dark/i', 'musick-to-play-in-the-dark'],
            ['/universo sonoro/i', 'universosonoro'],
            ['/QUEL CHE RESTA DELLA NOTTE/i', 'jene-nella-notte'],
            ['/qcrdn/i', 'jene-nella-notte'],
            ['/^Resetclub/i', 'reset-club'],
            ['/Radio Bizarre/i', 'radio-bizarre'],
            ['/la nave dei folli/i', 'la-nave-dei-folli-2'],
            ['/mystic sister/i', 'mystic-sister'],
            ['/la perla di labuan/i', 'la-perla-di-labuan'],
            ['/macerie su macerie/i', 'macerie-su-macerie'],
            ['/alliguai/i', 'alliguai'],
            ['/radio gorilla/i', 'radio-gorilla'],
            ['/radionotav/i', 'radionotav'],
            ['/radio notav/i', 'radionotav'],
            ['/harraga/i', 'harraga'],
            ['/animalradio/i', 'animalradio'],
            ['/malafemme/i', 'mala-femme'],
            ['/flip the beat/i', 'flip-the-beat'],
            ['/^black in/i', 'black-in'],
            ['/PENNY·KELLA/i', 'pennichella'],
            ['/ri-congiunzioni/i', 'congiunzioni'],
            ['/sono un ribelle mamma/i', 'sono-un-ribelle-mamma'],
            ['/metixflow/i', 'metix-flow'],
            ['/anarres/i', 'anarres'],
            ['/bello come una prigione che brucia/i', 'bello-come-una-prigione-che-brucia'],
            ['/19e59/i', '19e59'],
            ['/1959/i', '19e59'],
            ['/arsider/i', 'arsider'],
            ['/malormone/i', 'malormone'],
            ['/4x4 monster track/i', '4x4-monster-track'],
            ['/quelli della thc/i', 'quelli-della-thc'],
            ['/^trappola/i', 'trappola'],
            ['/^imballata/i', 'imballata-w-bubble-wrap']
        ];

        $newTerm = null;
        foreach($regexpMap as $regexp) {
            if (preg_match($regexp[0], $title)) {
                $newTerm = get_term_by('slug', $regexp[1], 'podcastfilter');
                break;
            }
        }
        
        if ($newTerm) {
            $show = get_page_by_path($newTerm->slug, OBJECT, ['shows']);
            if ($show) {
                return $newTerm->term_id;
            }
        }
    }

    function rbo_get_term_from_manual_id () {
        $idToPodcastfilter = [
            90504 => 'arsider',
            80354 => 'arsider',
            90750 => 'frittura-mista',
            85620 => 'frittura-mista',
            19914 => 'frittura-mista',
            19918 => 'frittura-mista',
            89927 => 'black-in',
            89659 => 'malormone',
            89553 => 'malormone',
            89547 => 'malormone',
            89363 => 'malormone',
            82269 => 'malormone',
            89209 => 'liberation-front',
            83038 => 'harraga',
            81437 => 'mala-femme',
            81443 => 'mala-femme',
            62122 => 'radionotav',
            82477 => 'radionotav',
            72957 => 'radionotav',
            73323 => 'no-trip-for-cats',
            76236 => 'congiunzioni',
            76007 => 'congiunzioni',
            48311 => 'liberation-front',
            77772 => 'black-in',
            74347 => 'flip-the-beat'
            // 85620 => 'congiunzioni'

        ];

        if (array_key_exists(get_the_ID(), $idToPodcastfilter)) {
            $newTerm = get_term_by('slug', $idToPodcastfilter[get_the_ID()], 'podcastfilter');
            $show = get_page_by_path($idToPodcastfilter[get_the_ID()], OBJECT, ['shows']);
            if ($show) {
                return $newTerm->term_id;
            }
        }        
    }

    function rbo_show_migration( $args ) {

        // per ogni trasmissione ci deve essere una tassonomia podcastfilter con lo stesso slug
        $shows = new WP_Query(['post_type' => 'shows', 'posts_per_page' => -1 ]);
        while ( $shows->have_posts() ) :
            $shows->the_post();
            $slug = get_post_field('post_name');
            $podcastfilter = get_term_by('slug', $slug, 'podcastfilter');
            if (!$podcastfilter) {
                // altrimenti devo creare una nuova tassonomia
                WP_CLI::success("Created new podcastfilter taxonomy: {$slug}");
                wp_create_term($slug, 'podcastfilter');
            }
        endwhile;

        $query = new WP_Query( array( 'post_type' => 'podcast', 'posts_per_page' => -1 ));

        WP_CLI::success( 'Podcast : ' . $query->found_posts);

        // per tutti questi podcast devo prendere tutti i terms (category, post_tag, podcastfilter)
        // dentro podcastfilter ne deve rimanere uno solo
        // se ne trovo uno che matcha o e' molto simile ad uno show post_type tengo quello
        // i restanti li aggiungo come tag!


        $retN = [ 'podcast' => $query->found_posts, 'meta' => 0, 'malformed_podcastfilter' => 0, 'podcastfilter' => 0, 'title' => 0, 'id' => 0, 'unknown' => 0];

        // per ogni podcast cerco lo show di riferimento
        while ( $query->have_posts() ) :

            $query->the_post();
            $showTermID = null;

            // provo con i meta
            $showTermID = rbo_get_term_from_meta();
            if ($showTermID) {
                $retN['meta']++;
            }

            // provo con i podcastfilter o i tag (sai mai..)
            if (!$showTermID) {
                $showTermID = rbo_get_term_from_podcastfilter();
                if ($showTermID) {
                    $retN['podcastfilter']++;
                }
            }

            // se non ho ancora trovato uno show di riferimento, uso le mappe alla ricerca di un podcastfilter mal formato
            if (!$showTermID) {
                $showTermID = rbo_get_term_from_malformed_podcastfilter();
                if ($showTermID) {
                    $retN['malformed_podcastfilter']++;
                }
            }

            // non ho trovato ancora lo show, uso il titolo come penultima spiaggia
            if (!$showTermID) {
                $showTermID = rbo_get_term_from_title();
                if ($showTermID) {
                    $retN['title']++;
                }
            }

            // uso una mappatura manuale
            if (!$showTermID) {
                $showTermID = rbo_get_term_from_manual_id();
                if ($showTermID) {
                    $retN['id']++;
                }
            }

            // NO, non uso l'autore come ultima spiaggia: troppi falsi positivi
            // $terms = wp_get_object_terms( get_the_ID(), ['podcastfilter', 'post_tag']);

            // wp_set_object_terms(get_the_ID(), array_map(function($t) { return $t->term_id; }, $terms), 'post_tag');

            if ($showTermID) {
                wp_set_object_terms(get_the_ID(), intval($showTermID), 'podcastfilter');
            } else {
                // echo "[WARNING] Non ho trovato lo show relativo: " . get_the_title() . " " . get_the_permalink() . "\n";
                $retN['unknown']++;
                // wp_set_object_terms(get_the_ID(), [], 'podcastfilter');
            }

            // if (!$showTermID) {
            //     echo get_the_author_meta('display_name') . 'id: ' . get_the_ID() . ' ' . get_the_title() . ' ' . get_the_permalink() . "\n";
            //     echo implode(',', array_map(function($a) { return $a->slug; }, $terms));
            //     echo "\n\n";
            //     // echo $podcastfilters . ' ' . $tags;
            // }
        endwhile;
        print_r($retN);
    }

    function rbo_add_timeslots( $args ) {
        
        $mapShows = [

            // lunedi
            'matine-xxl' => [['day' => 'lunedi', 'start' => '07:00', 'end' => '09:00']],
            'bello-come-una-prigione-che-brucia' => [['day' => 'lunedi', 'start' => '11:00', 'end' => '13:00']],
            'liberation-front' => [['day' => 'lunedi', 'start' => '13:00', 'end' => '14:30']],
            'aguaplano' => [['day' => 'lunedi', 'start' => '14:30', 'end' => '16:00']],
            'flip-the-beat' => [['day' => 'lunedi', 'start' => '16:00', 'end' => '17:00']],
            'sono-un-ribelle-mamma' => [['day' => 'lunedi', 'start' => '17:00', 'end' => '18:30']],
            'black-in' => [['day' => 'lunedi', 'start' => '18:30', 'end' => '19:30']],
            'macerie-su-macerie' => [['day' => 'lunedi', 'start' => '19:30', 'end' => '20:30']],
            'radio-kalakuta' => [['day' => 'lunedi', 'start' => '20:30', 'end' => '22:00']],
            'malormone' => [['day' => 'lunedi', 'start' => '22:00', 'end' => '00:00']],

            // martedi
            'universo-sonoro' => [['day' => 'martedi', 'start' => '13:00', 'end' => '15:00']],
            'piovono-calcinacci' => [['day' => 'martedi', 'start' => '15:00', 'end' => '16:30']],
            'overjoy' => [['day' => 'martedi', 'start' => '16:30', 'end' => '18:30']],
            'congiunzioni' => [['day' => 'martedi', 'start' => '18:30', 'end' => '19:30']],
            'frittura-mista' => [['day' => 'martedi', 'start' => '19:30', 'end' => '21:30']],
            'riot-on-sunset-strip' => [['day' => 'martedi', 'start' => '21:30', 'end' => '23:00']],
            'musick-to-play-in-the-dark' => [['day' => 'martedi', 'start' => '23:00', 'end' => '00:00']],
            
            // mercoledi'
            'stakka-stakka' => [['day' => 'mercoledi', 'start' => '11:00', 'end' => '12:30']],
            'pennichella' => [['day' => 'mercoledi', 'start' => '12:30', 'end' => '14:00']],
            'chai-le-storie' => [['day' => 'mercoledi', 'start' => '15:30', 'end' => '17:00']],
            'spot' => [['day' => 'mercoledi', 'start' => '17:30', 'end' => '18:30']],
            'acab' => [['day' => 'mercoledi', 'start' => '18:30', 'end' => '20:00']],
            'radiokebab' => [['day' => 'mercoledi', 'start' => '20:00', 'end' => '22:00']],
            'tigre-vs-tigre' => [['day' => 'mercoledi', 'start' => '22:00', 'end' => '24:00']],

            // giovedi'
            'mala-femme' => [['day' => 'giovedi', 'start' => '11:00', 'end' => '13:00']],
            'radionotav' => [['day' => 'giovedi', 'start' => '13:00', 'end' => '14:30']],
            'imballata-w-bubble-wrap' => [['day' => 'giovedi', 'start' => '15:30', 'end' => '16:30']],
            'blackmamba' => [['day' => 'giovedi', 'start' => '16:30', 'end' => '18:30']],
            'ristoradio' => [['day' => 'giovedi', 'start' => '18:30', 'end' => '20:00']],
            'i-bastioni-di-orione' => [['day' => 'giovedi', 'start' => '20:00', 'end' => '21:30']],
            'healing-frequencies' => [['day' => 'giovedi', 'start' => '21:30', 'end' => '22:30']],
            'arsider' => [['day' => 'giovedi', 'start' => '22:30', 'end' => '00:00']],

            // venerdi'
            'anarres' => [['day' => 'venerdi', 'start' => '11:00', 'end' => '13:00']],
            'harraga' => [['day' => 'venerdi', 'start' => '14:30', 'end' => '16:00']],
            'aria' => [['day' => 'venerdi', 'start' => '16:00', 'end' => '17:00']],
            'il-giornale-malandrino' => [['day' => 'venerdi', 'start' => '17:00', 'end' => '18:30']],
            'metix-flow' => [['day' => 'venerdi', 'start' => '18:30', 'end' => '20:00']],
            'trigger-point' => [['day' => 'venerdi', 'start' => '20:00', 'end' => '21:00']],
            'a-scatola-chiusa' => [['day' => 'venerdi', 'start' => '21:30', 'end' => '23:00']],
            '4x4-monster-track' => [['day' => 'venerdi', 'start' => '23:00', 'end' => '24:00']],
            'reset-club' => [['day' => 'venerdi', 'start' => '24:00', 'end' => '02:00']],

            // sabato
            'radio-bizarre' => [['day' => 'sabato', 'start' => '10:30', 'end' => '12:00']],
            'alliguai' => [['day' => 'sabato', 'start' => '12:00', 'end' => '13:30']],
            'umoradio' => [['day' => 'sabato', 'start' => '13:30', 'end' => '15:00']],
            'black-milk' => [['day' => 'sabato', 'start' => '15:00', 'end' => '18:00']],
            'tanz-bambolina' => [['day' => 'sabato', 'start' => '18:00', 'end' => '19:30']],
            'mystic-sister' => [['day' => 'sabato', 'start' => '19:30', 'end' => '20:30']],
            'backwards' => [['day' => 'sabato', 'start' => '21:30', 'end' => '23:00']],

            // domenica
            'matinata' => [['day' => 'domenica', 'start' => '11:00', 'end' => '13:00']],
            'assadaka-amicizia' => [['day' => 'domenica', 'start' => '14:00', 'end' => '17:00']],
            'fino-allultimo-respiro' => [['day' => 'domenica', 'start' => '19:00', 'end' => '21:00']],
            'no-trip-for-cats' => [['day' => 'domenica', 'start' => '21:00', 'end' => '22:30']],
            'jene-nella-notte' => [['day' => 'domenica', 'start' => '22:30', 'end' => '02:00']],

        ];
        $shows = new WP_Query([ 'post_type' => 'shows', 'posts_per_page' => -1 ]);
        while ($shows->have_posts()) :
            $shows->the_post();
            $slug = get_post_field('post_name');
            if (!array_key_exists($slug, $mapShows)) {
                echo "No party - {$slug}\n";
                // exit -1;
            } else {
                update_post_meta(get_the_ID(), 'slot_active', true);
                update_post_meta(get_the_ID(), 'timeslots', $mapShows[$slug]);
            }
        endwhile;
    }

    WP_CLI::add_command( 'rbo_show_migration', 'rbo_show_migration', 'RBO show migration');
    WP_CLI::add_command( 'rbo_add_timeslots', 'rbo_add_timeslots', 'RBO add timeslots');
}
