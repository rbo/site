<?php
defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );

add_action('init', 'rbo_show_register_type');  
// add_action('rest_api_init', 'rbo_show_custom_fields');
add_action( 'add_meta_boxes', 'rbo_timeslot_metaboxes' ); 
add_action( 'save_post', 'rbo_timeslot_save' );

add_filter( 'manage_shows_posts_columns', 'shows_filter_posts_columns' );
add_action( 'manage_shows_posts_custom_column', 'shows_column', 10, 2);

function shows_column($column ,$post_id) {
  if ( 'slot_day' === $column ) {
    echo (get_post_meta($post_id, 'slot_active', true) === 'on' ? 'Attiva - ' : 'Non attiva - ') .
      get_post_meta($post_id, 'slot_day', true) . ' dalle ' .
      get_post_meta($post_id, 'slot_start', true) . ' alle ' . 
      get_post_meta($post_id, 'slot_end', true);
  }
}

function shows_filter_posts_columns( $columns ) {
  $columns['slot_day'] = __( 'Giorno' );
  return $columns;
}

function rbo_show_register_type () {
  
  $labelsshow = array(
    'name' => esc_attr__("Trasmissioni"),
    'singular_name' => esc_attr__("Trasmissione"),
    'add_new' => esc_attr__("Aggiungi nuova"),
    'add_new_item' => esc_attr__("Aggiungi nuova trasmissione"),
    'edit_item' => esc_attr__("Modifica trasmissione"),
    'new_item' => esc_attr__("Nuova trasmissione"),
    'all_items' => esc_attr__("Tutte le trasmissioni"),
    'view_item' => esc_attr__("Mostra trasmissione"),
    'search_items' => esc_attr__("Cerca trasmissione"),
    'not_found' => esc_attr__("Trasmissione non trovata"),
    'not_found_in_trash' => esc_attr__("No shows found in trash"),
    'menu_name' => esc_attr__("Trasmissioni")
  );
  $args = array(
    'labels' => $labelsshow,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'page',
    'has_archive' => true,
    'hierarchical' => true,
    'menu_position' => 50,
    'show_in_rest' => true,
    'page-attributes' => true,
    'show_in_nav_menus' => true,
    'show_in_admin_bar' => true,
    'taxonomies' => array('post_tag'),
    'menu_icon' =>  'dashicons-pressthis',
    'supports' => array('title', 'thumbnail','editor', 'excerpt', 'revisions')
  );     
  register_post_type( 'shows', $args);
  // register_taxonomy('shows',array('shows', 'podcast'), $args);
  
  register_post_meta('shows', 'timeslots', [
    'type' => 'object',
    'description' => "Gli slot temporali assegnati alla trasmissione",
    'single' => true,
    'default' => [],
    'show_in_rest' => true
  ]);

  

  register_post_meta('shows', 'slot_day', [
    'type' => 'string',
    'description' => __("Day of the week in which the show is scheduled", "onair2"),
    'single' => true
  ]);

  register_post_meta('shows', 'slot_start', [
    'type' => 'string',
    'description' => __("Hour at which the show is scheduled", "onair2"),
    'single' => true
  ]);

  register_post_meta('shows', 'slot_end', [
    'type' => 'string',
    'description' => __("Hour at hich the show ends", "onair2"),
    'single' => true
  ]);
}

// function rbo_show_custom_fields () {
//   register_rest_field( 'shows', 'timeslots', [
//       'get_callback'    => 'get_timeslots',
//       'schema'          => null //TODO
//   ]);
//   register_rest_field('shows', 'active', [
//     'get_callback' => 'get_active',
//     'schema' => null
//   ]);
// }

function get_timeslots ( $object ) {
  if ($object['post_type'] = 'shows') {
    return get_post_meta($object['id'], 'timeslots', true);
    /*
    return [
        [
          'day' => get_post_meta($object['id'], 'slot_day', true),
          'start' => get_post_meta($object['id'], 'slot_start', true),
          'end' => get_post_meta($object['id'], 'slot_end', true)
        ]
    ];15 e 16 Novembre. 
    */
  }
  return null;
}
  
function rbo_timeslot_save ( $post_id ) {
  // if ( ! isset( $_POST['nonce'] ) ) {
	// 	return;
	// }

	// if ( ! wp_verify_nonce( $_POST['nonce'], 'nonce_value' ) ) {
	// 	return;
	// }

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
  $days = ['lunedi', 'martedi', 'mercoledi', 'giovedi', 'venerdi', 'sabato', 'domenica'];

  if ( isset($_POST['timeslot_day']) && in_array($_POST['timeslot_day'],$days)) {
    update_post_meta($post_id, 'slot_day', $_POST['timeslot_day']);
  }

  if ( isset($_POST['timeslot_start']) && preg_match('/^[0-9]{2}:[0-9]{2}/', $_POST['timeslot_start'])) {
    update_post_meta($post_id, 'slot_start', $_POST['timeslot_start']);
  }

  if ( isset($_POST['timeslot_end']) && preg_match('/^[0-9]{2}:[0-9]{2}/', $_POST['timeslot_end'])) {
    update_post_meta($post_id, 'slot_end', $_POST['timeslot_end']);
  }

  if ( isset($_POST['timeslot_day'])
    && isset($_POST['timeslot_start'])
    && isset($_POST['timeslot_end'])
    && in_array($_POST['timeslot_day'],$days)
    && preg_match('/^[0-9]{2}:[0-9]{2}/', $_POST['timeslot_start'])
    && preg_match('/^[0-9]{2}:[0-9]{2}/', $_POST['timeslot_end'])
  ) {
    $slot = get_post_meta($post_id, 'timeslots', true);
    $slot[0] = [
      'day' => $_POST['timeslot_day'],
      'start' => $_POST['timeslot_start'],
      'end' => $_POST['timeslot_ent']
    ];
    update_post_meta($post_id, 'timeslots', $slot);
  }

  if ( isset($_POST['timeslots'])) {
    // $timeslots = array_filter($_POST['timeslots'], function ($e) {
    //   return in_array( $e['day'], ['lunedi', 'martedi', 'mercoledi', 'giovedi', 'venerdi', 'sabato', 'domenica']);
    // });
    update_post_meta($post_id, 'timeslots', $_POST['timeslots']);
  }

  // update_post_meta($post_id, 'slot_active', $_POST['timeslot_active'] ?? false);

}

function rbo_timeslot_display ( $post ) {
  // $palinsesto = wp_get_object_terms( $post->ID, 'orario', array( 'orderby' => 'term_id', 'order' => 'ASC' ) );
  ?>
  <?php
    $day_options = [
      'lunedi' => ['name' => 'Lunedì', 'selected' => false],
      'martedi' => ['name' => 'Martedì', 'selected' => false],
      'mercoledi' => ['name' => 'Mercoledì', 'selected' => false],
      'giovedi' => ['name' => 'Giovedì', 'selected' => false],
      'venerdi' => ['name' => 'Venerdì', 'selected' => false],
      'sabato' => ['name' => 'Sabato', 'selected' => false],
      'domenica' => ['name' => 'Domenica', 'selected' => false]
    ];
    $timeslots = get_post_meta($post->ID, 'timeslots', true);

    // print_r($timeslots);
    if (!is_array($timeslots)) {
      if ($timeslots) {
        //print_r($timeslots);
      }
      $timeslots = [];
    }
    /*
    $selected_day = get_post_meta($post->ID, 'slot_day', true);
    if (is_string($selected_day) && array_key_exists($selected_day, $day_options)) {
      $day_options[$selected_day]['selected'] = true;
    }
    $selected_start = get_post_meta($post->ID, 'slot_start', true);
    $selected_end = get_post_meta($post->ID, 'slot_end', true);
    */
    $selected_active = get_post_meta($post->ID, 'slot_active', true);
    // $length = count($timeslots);

    echo "<div>";

    // trasmissioni speciali con piu' fasce orarie (info, bobina?, black holes?)
    if (count($timeslots) > 1) {
      echo "Trasmissione speciale?</div>";
      return;
    }
    print_r($timeslots);
  ?>
  <label>Giorno:
    <select name="timeslots[0][day]" class="components-form-token-field__input">
      <option value="none" selected>Nessuno</option>
      <?php foreach ($day_options as $value => $day) { ?>
        <option value="<?php echo $value ?>"><?php echo $day['name'] ?></option>
      <?php } ?>
    </select>
  </label><br/><br/>
  <label>
    Ora inizio:
    <input type="time" name="timeslots[0][start]" />
  </label><br/>
  <label>
    Ora fine:
    <input type="time" name="timeslots[0][end]" />
  </label><br/>
<label>Attiva:
    <input type="checkbox" name="timeslot_active" <?php echo $selected_active === 'on' ? 'checked' : '' ?>/>
  </label>
  </div>
  <?php
    
  
}

function rbo_timeslot_metaboxes() {
  if (is_admin_bar_showing()) {
    add_meta_box('rbo_timeslot_metabox', __('Quando'), 'rbo_timeslot_display', 'shows', 'side');
  }
}

?>