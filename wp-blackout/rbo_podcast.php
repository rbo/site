<?php

defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );

add_action('init', 'rbo_podcast_register_type');  
add_filter( 'rest_prepare_podcast', 'my_custom_json_fields', 12, 3 );
add_filter( 'rest_prepare_post', 'my_custom_json_fields', 12, 3 );
add_filter( 'rest_prepare_shows', 'my_custom_json_fields', 120, 3 );

function rbo_podcast_register_type() {
  $labelspodcast = array(
    'name' => esc_attr__("Podcast","rbo"),
    'singular_name' => esc_attr__("Podcast","rbo"),
    'add_new' => esc_attr__("Add new","rbo"),
    'add_new_item' => esc_attr__("Add new podcast","rbo"),
    'edit_item' => esc_attr__("Edit podcast","rbo"),
    'new_item' => esc_attr__("New podcast","rbo"),
    'all_items' => esc_attr__("All podcasts","rbo"),
    'view_item' => esc_attr__("View podcast","rbo"),
    'search_items' => esc_attr__("Search podcast","rbo"),
    'not_found' => esc_attr__("No podcasts found","rbo"),
    'not_found_in_trash' => esc_attr__("No podcasts found in trash","rbo"),
    'menu_name' => esc_attr__("Podcast","rbo")
  );
  
  $args = array(
    'labels' => $labelspodcast,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => [ "slug" => "podcast" ],
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 30,
    'show_in_rest' => true,
    'page-attributes' => true,
    'show_in_nav_menus' => true,
    'show_in_admin_bar' => true,
    'taxonomies' => array('post_tag', 'show', 'podcastfilter'),
    'menu_icon' => 'dashicons-megaphone',
    'supports' => ['title', 'thumbnail','editor', 'custom-fields', 'excerpt']
  ); 
  
  // add new podcast custom_type
  register_post_type( "podcast" , $args );
  // register_taxonomy('show', ['podcast']);
  
  
  
  /* ============= create custom taxonomy for the podcasts ==========================*/
  $labels = array(
    'name' => esc_attr__( 'Trasmissioni',"onair2" ),
    'singular_name' => esc_attr__( 'Trasmissione',"onair2" ),
    'search_items' =>  esc_attr__( 'Search by filter',"onair2" ),
    'popular_items' => esc_attr__( 'Popular filters',"onair2" ),
    'all_items' => esc_attr__( 'All Podcasts',"onair2" ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => esc_attr__( 'Edit Filter',"onair2" ), 
    'update_item' => esc_attr__( 'Update Filter',"onair2" ),
    'add_new_item' => esc_attr__( 'Aggiungi trasmissione',"onair2" ),
    'new_item_name' => esc_attr__( 'New Filter Name',"onair2" ),
    'separate_items_with_commas' => esc_attr__( 'Separate Filters with commas',"onair2" ),
    'add_or_remove_items' => esc_attr__( 'Add or remove Filters',"onair2" ),
    'choose_from_most_used' => esc_attr__( 'Choose from the most used Filters',"onair2" ),
    'menu_name' => esc_attr__( 'Filters',"onair2" ),
  ); 
  
  $args = array(
    // 'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'update_count_callback' => '_update_post_term_count',
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'podcastfilter' )
  );

  register_taxonomy('podcastfilter',['podcast'], $args );
  
  add_action( 'add_meta_boxes', 'rbo_show_metaboxes' ); 
  function rbo_show_metaboxes () {
    add_meta_box('rbo_show_metabox', __('Trasmissione'), 'post_shows_meta_box', 'podcast', 'side');
  }
  
  // add featured_img_src into REST API
  // register_rest_field( 'podcast', 'featured_image_src', array(
  //   'get_callback' => function ( $post_arr ) {
  //     $image_src_arr = wp_get_attachment_image_src( $post_arr['featured_media'], 'medium' );
  
  //     return $image_src_arr[0];
  //   },
  //   'update_callback' => null,
  //   'schema' => null
  // ) );
  
  // register_rest_field('podcast', 'show', [
  //   'get_callback' => function ($post_arr) {
  //     return get_post_meta($post_arr['id'], 'show', true);
  //   }
  // ]);
  
  function my_custom_json_fields( $data, $post, $request ) {
    if (is_admin()) { // if not is admin
      return $data;
    }

    $terms = wp_get_object_terms( $post->ID, ['podcastfilter', 'post_tag', 'category'], array( 'orderby' => 'term_id', 'order' => 'ASC' ) );
    $featured_media = get_post_meta( $data->data['featured_media'], '_wp_attachment_metadata', true );
    
    $the_excerpt = $data->data['content']['rendered']; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 60; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);
    
    if(count($words) > $excerpt_length) :
      array_pop($words);
      array_push($words, '…');
      $the_excerpt = implode(' ', $words);
    endif;
    
    $show = null;
    $tags = [];
    $category = null;
    
    foreach ($terms as $term) {
       if ($term->taxonomy == 'podcastfilter' && ! $show)  {
        // $show = array( 'name' => $term->name, 'slug' => $term->slug);
        $show = get_page_by_path($term->slug, OBJECT, ['shows']);
      } else if ($term->taxonomy == 'post_tag') {
        array_push($tags, array( 'name' => $term->name, 'slug' => $term->slug, 'id' => $term->term_id ));
      } else if ($term->taxonomy == 'category') {
        $category = array( 'name' => $term->name, 'slug' => $term->slug);
      }
    }
    
    // $data->data['featured_media_path'] = empty($featured_media['file']) ? null : $featured_media['file'];
    // $data->data['timeslots'] = [[
    //   'day' => get_post_meta($post->ID, 'slot_day', true),
    //   'start' => get_post_meta($post->ID, 'slot_start', true),
    //   'end' => get_post_meta($post->ID, 'slot_end', true)
    // ]];
      
    if ($show) {
      $show = ['slug' => $show->post_name, 'name' => $show->post_title];
    }
    // if ($tags) {
    //   $data->data['tags'] = $tags;
    // }
    // return $data;
    $timeslots = get_post_meta($post->ID, 'timeslots');
    if (count($timeslots)) {
      $timeslots = $timeslots[0];
    }

    return [
        'id'      => $post->ID,
        'title'    => $data->data['title']['rendered'],
        'slug' => $data->data['slug'],
        'date' => $data->data['date'],
        'content' => $data->data['content']['rendered'],
        'excerpt' => get_the_excerpt($post->ID),// $the_excerpt,
        'show' => $show,
        'tags' => $tags,
        'active' => get_post_meta($post->ID, 'slot_active', true) || false,
        'category' => $category,
        'timeslots' => $timeslots,
        'featured_media' => empty($featured_media['file']) ? null : $featured_media['file']
      ];
    }
    
    
    add_filter( 'rest_podcast_query', function( $args, $request ) {
      $show   = $request->get_param( 'show' );
      
      if ( ! empty( $show ) ) {
        $args['tax_query'] = array(
          array(
            'taxonomy'     => 'podcastfilter',
            'field'   => 'slug',
            'terms' => $show)
          );      
        }
        return $args;
      }, 10, 2 );


      function cm_typesense_add_available_post_types( $available_post_types ) {
        $available_post_types['podcast'] = [ 'label' => 'Podcast', 'value' => 'podcast' ];
        return $available_post_types;
      }
      add_filter( 'cm_typesense_available_index_types',  'cm_typesense_add_available_post_types');
      
  //     //only necessary if the default post schema is not necessary
      function cm_typesense_add_podcast_schema( $schema, $name ) {
        if ( $name == 'podcast' ) {
            $schema = [
              'name'                  => 'podcast',
              'fields'                => [
                [ 'name' => 'post_content', 'type' => 'string' ],
                [ 'name' => 'post_title', 'type' => 'string' ],
                [ 'name' => 'post_type', 'type' => 'string' ],
                [ 'name' => 'post_author', 'type' => 'string', 'optional' => true, 'facet' => true ],
                [ 'name' => 'comment_count', 'type' => 'int64' ],
                [ 'name' => 'is_sticky', 'type' => 'int32' ],
                [ 'name' => 'post_excerpt', 'type' => 'string' ],
                [ 'name' => 'post_date', 'type' => 'string' ],
                [ 'name' => 'sort_by_date', 'type' => 'int64' ],
                [ 'name' => 'post_id', 'type' => 'string' ],
                [ 'name' => 'post_modified', 'type' => 'string' ],
                // [ 'name' => 'id', 'type' => 'string' ],
                [ 'name' => 'permalink', 'type' => 'string' ],
                [ 'name' => 'post_thumbnail', 'type' => 'string', 'optional' => true, 'index' => false ],
                // [ 'name' => 'post_thumbnail_html', 'type' => 'string', 'optional' => true, 'index' => false ],
                // [ 'name' => 'category', 'type' => 'string[]', 'optional' => true, 'facet' => true ],
                // [ 'name' => 'cat_link', 'type' => 'string[]', 'optional' => true, 'index' => false ],
                [ 'name' => 'tags', 'type' => 'string[]', 'optional' => true, 'facet' => true ],
                [ 'name' => 'podcastfilter', 'type' => 'string[]', 'facet' => true ],
                // [ 'name' => 'tag_links', 'type' => 'string[]', 'optional' => true, 'index' => false ],
    
              ],
              'default_sorting_field' => 'sort_by_date',
            ];
        }
      
      
        return $schema;
      }
      
      add_filter( 'cm_typesense_schema', 'cm_typesense_add_podcast_schema', 10, 2 );

      // TODO: format post_date in italian? permalink and thumbnail without baseurl 
      function cm_typesense_format_podcast_data ( $formatted_data, $raw_data, $object_id, $schema_name ) {
        if ( $schema_name == 'podcast' ) {
            $terms  = get_the_terms( $object_id, 'podcastfilter' );
            $genres = [];
            foreach ( $terms as $term ) {
                $genres[] = $term->name;
            }
            $formatted_data['podcastfilter'] = $genres;
        }
    
        return $formatted_data;
    }
    add_filter( 'cm_typesense_data_before_entry', 'cm_typesense_format_podcast_data', 10, 4 );
  }
    
    
    
    
    // function show_required_field_error_msg( $post ) {
    // 	if ( 'podcast' === get_post_type( $post ) && 'auto-draft' !== get_post_status( $post ) ) {
    //     if ( ! $post->post_parent ) {
    //       printf(
    //         '<div class="error below-h2"><p>%s</p></div>',
    //         esc_html__( 'Rating is mandatory for creating a new movie post' )
    //       );
    //     }
    //   }
    // }
    // Unfortunately, 'admin_notices' puts this too high on the edit screen
    // add_action( 'edit_form_top', 'show_required_field_error_msg' );
    
    /**
    * Save the meta when the post is saved.
    *
    * @param int $post_id The ID of the post being saved.
    */
    function rbo_save( $post_id ) {
      
      /*
      * We need to verify this came from the our screen and with proper authorization,
      * because save_post can be triggered at other times.
      */
      
      // Check if our nonce is set.
      // if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) ) {
      //     return $post_id;
      // }
      
      // $nonce = $_POST['myplugin_inner_custom_box_nonce'];
      
      // Verify that the nonce is valid.
      // if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) ) {
      //     return $post_id;
      // }
      
      /*
      * If this is an autosave, our form has not been submitted,
      * so we don't want to do anything.
      */
      // if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
      //     return $post_id;
      // }
      
      // // Check the user's permissions.
      // if ( 'page' == $_POST['post_type'] ) {
      //     if ( ! current_user_can( 'edit_page', $post_id ) ) {
      //         return $post_id;
      //     }
      // } else {
      //     if ( ! current_user_can( 'edit_post', $post_id ) ) {
      //         return $post_id;
      //     }
      // }
      
      /* OK, it's safe for us to save the data now. */
      
      // Sanitize the user input.
      // $mydata = sanitize_text_field( $_POST['show'] );
      // $show = get_post($_POST['show']);
      
      
      $term = get_term_by( 'slug', $_POST['show'], 'podcastfilter' );
      if ( empty( $term ) || is_wp_error( $term ) ) {
        $show = get_page_by_path($_POST['show'], OBJECT, ['shows']);
        $term = wp_insert_term($show->name, 'podcastfilter', array('slug' => $show->slug));
      }
      wp_set_object_terms( $post_id, $term->term_id, 'podcastfilter', false );
      // Update the meta field.
      // update_post_meta( $post_id, 'show_id', $show->ID);
      // update_post_meta( $post_id, 'show', array(
      //   'id' => $show->ID,
      //   'title' => $show->post_title,
      //   'name' => $show->post_name,
      
      // ));
      // remove_action('save_post', 'rbo_save');
      // wp_update_post(array('ID' => $post_id, 'post_parent' => $_POST['show']));
      // add_action( 'save_post', 'rbo_save');
      
    }
    
    add_action( 'save_post_podcast', 'rbo_save');
    
    
    function post_shows_meta_box( $post ) {
      // $show_id = get_post_meta( $post->ID, 'show_id', true );
      // $term = get_term_by( 'slug', 'frittura-mista', 'podcastfilter' );
      // $terms = get_terms( 'podcastfilter', array( 'hide_empty' => false ) );
      // $post  = get_post();
      // get_post_ter
      $podcastfilter = wp_get_object_terms( $post->ID, 'podcastfilter', array( 'orderby' => 'term_id', 'order' => 'ASC' ) );
      $show = get_page_by_path($podcastfilter[0]->slug, OBJECT, ['shows']);
      ?>
      
      <div style="display: flex; max-width: 200px;">
      <?php
        $args = array(
          'post_type' => 'shows',
          'value' => 'show',
          'value_field' => 'post_name',
          'selected' => isset($show->ID) ? $show->ID : null,
          'name' => 'show',
          'show_option_none' => '-- Che trasmissione ? --',
          'echo' => 1);
        
        wp_dropdown_pages($args);
      ?>
      </div>
        <?php
      }