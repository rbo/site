<?php


defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );

// Add media custom field
add_action('init', 'rbo_add_attachment_taxonomies');

function rbo_add_attachment_taxonomies () {
  register_taxonomy_for_object_type( 'podcast', 'attachment');
  register_taxonomy_for_object_type( 'post', 'attachment');

	$labels = array(
		'name' => esc_attr__( 'Replica',"onair2" ),
		'singular_name' => esc_attr__( 'Replica',"onair2" ),
		'search_items' =>  esc_attr__( 'Search by replica',"onair2" ),
		'popular_items' => esc_attr__( 'Popular replica',"onair2" ),
		'all_items' => esc_attr__( 'All Podcasts',"onair2" ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => esc_attr__( 'Edit Filter',"onair2" ), 
		'update_item' => esc_attr__( 'Update Filter',"onair2" ),
		'add_new_item' => esc_attr__( 'Add New Filter',"onair2" ),
		'new_item_name' => esc_attr__( 'New Filter Name',"onair2" ),
		'separate_items_with_commas' => esc_attr__( 'Separate Filters with commas',"onair2" ),
		'add_or_remove_items' => esc_attr__( 'Add or remove Filters',"onair2" ),
		'choose_from_most_used' => esc_attr__( 'Choose from the most used Filters',"onair2" ),
		'menu_name' => esc_attr__( 'Filters',"onair2" ),
	); 

	$args = array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
    'show_in_rest' => true,
		// 'update_count_callback' => '_update_post_term_count',
    // 'meta_box_cb' => 'post_shows_meta_box',
    // 'show_admin_column' => true,
		'query_var' => true,
    'rewrite' => true,
		// 'rewrite' => array( 'slug' => 'show' )
	);

  register_taxonomy('replicable_attachment','attachment', $args );

}
