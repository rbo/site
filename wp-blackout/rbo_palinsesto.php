<?php

/** 
 * /wp-json/rbo/v1/palinsesto
 * returns 
 **/
add_action( 'rest_api_init', function () {
  register_rest_route( 'rbo/v1', '/palinsesto', array(
    'methods' => 'GET',
    'callback' => 'rbo_palinsesto',
    'permission_callback' => '__return_true')
  );

  register_rest_route( 'rbo/v1', '/listening', array(
    'methods' => 'GET',
    'callback' => 'rbo_listening',
    'permission_callback' => '__return_true')
  );

});


function rbo_listening () {

  // get current day and time
  $weekday = date('w');
  $days = ['domenica', 'lunedi', 'martedi', 'mercoledi', 'giovedi', 'venerdi', 'sabato'];

  $posts = new WP_Query([
    'post_type' => 'shows',
    'posts_per_page' => -1
  ]);

  // for each show
  while( $posts->have_posts() ) {
    $posts->the_post();
    $timeslots = get_post_meta(get_the_ID(), 'timeslots', true);
    foreach($timeslots as $slot) {
      $day = $slot['day'];
    }
  }
}


function rbo_palinsesto () {
  $posts = new WP_Query([
    'post_type' => 'shows',
    'posts_per_page' => -1
  ]);
  
  $timetable = [];
  
  while( $posts->have_posts() ) {
    $posts->the_post();
    $timeslots = get_post_meta(get_the_ID(), 'timeslots', true);
    $tag_query = get_the_tags();
    $tags = [];
    if ($tag_query && !is_wp_error($tag_query)) {
      foreach($tag_query as $tag) {
        $tags[] = [
          'slug' => $tag->slug,
          'name' => $tag->name
        ];
      }
    }
    
    $featured_media = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_metadata', true );
    if (!count($timeslots)) continue;

    foreach($timeslots as $slot) {
      $day = $slot['day'];
      $timetable[$day][] = [
        'title' => get_the_title(),
        'slug' => get_post_field('post_name'),
        'excerpt' => get_the_excerpt(get_the_ID()),
        'featured_media' => empty($featured_media['file']) ? null : $featured_media['file'],
        'timeslots' => $timeslots,
        'tags' => $tags
        ];
    }
  }

  uksort($timetable, function ($a, $b) {
    $days = ['lunedi', 'martedi', 'mercoledi', 'giovedi', 'venerdi', 'sabato', 'domenica'];
    $a_k = array_search($a, $days);
    $b_k = array_search($b, $days);
    $a_k = is_int($a_k) ? $a_k : 9;
    $b_k = is_int($b_k) ? $b_k : 9;
    return $a_k - $b_k;
  });

  $days = ['lunedi', 'martedi', 'mercoledi', 'giovedi', 'venerdi', 'sabato', 'domenica'];
  foreach ($days as $_idx => $day) {
    if (!key_exists($day, $timetable)) continue;
    usort($timetable[$day], function($a, $b) {
      return intval($a['timeslots'][0]['start']) - intval($b['timeslots'][0]['start']);
    });
  }

  return $timetable;
}
