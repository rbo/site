<?php

defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );

add_action('init', 'rbo_event_register_type');

function rbo_event_register_type () {
  
  $labelsevent = array(
    'name' => esc_attr__("Eventi","onair2"),
    'singular_name' => esc_attr__("Eventi","onair2"),
    'add_new' => esc_attr__("Add new","onair2"),
    'add_new_item' => esc_attr__("Add new event","onair2"),
    'edit_item' => esc_attr__("Edit event","onair2"),
    'new_item' => esc_attr__("New event","onair2"),
    'all_items' => esc_attr__("All events","onair2"),
    'view_item' => esc_attr__("View event","onair2"),
    'search_items' => esc_attr__("Search event","onair2"),
    'not_found' => esc_attr__("No events found","onair2"),
    'not_found_in_trash' => esc_attr__("No events found in trash","onair2"),
    'menu_name' => esc_attr__("Events","onair2")
  );
  $args = array(
    'labels' => $labelsevent,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_rest' => true,
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'page',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 30,
    'menu_icon' => 'dashicons-calendar-alt',
    'page-attributes' => true,
    'show_in_nav_menus' => true,
    'taxonomies' => array('post_tag'),
    'show_in_admin_bar' => true,
    'show_in_menu' => true,
    'supports' => array('title','thumbnail','editor','page-attributes')
  ); 
  register_post_type( 'event', $args);
  
}
?>
