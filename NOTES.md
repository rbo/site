## Struttura dati e WP
WordPress e' il CMS che ereditiamo ed e' il modo in cui le persone in radio pubblicano i contenuti dal 2005 quindi per mantenere lo storico dei contenuti, un'interfaccia a cui
in radio le persone sono abituate e soprattutto della sanita' mentale non cambieremo.
L'idea e' di separare almeno la parte visibile pubblicamente perche' difficile nel tempo da mantenere (il sito e' passato attraverso vari temi, a volte abbandonati, a volte scaduti, a volte passati a pagamento).

WP ha dei post type di default (`post` e `page`) e delle tassonomie associate (`tags` e `categories`) a cui e' possibile aggiungerne altre manualmente.
Nel tempo sono stati utilizzati dei plugin e temi per supportare diversi post type per il caso specifico di una radio, quindi un'entita' `podcast`
e un'entita' `shows` (le trasmissioni) insieme ad altre entita' corollarie (ad esempio `palinsesto`), il tutto in modo non chiarissimo.

Razionalizando bisognera' avere invece i seguenti post_type:
- i normalissimi `post` che sono sempre stati prerogativa dell'info e rimarranno tali.
- le normalissime `page` che possono essere utilizzate per creare pagine custom modificabili da WP (vedi ad esempio la pagina contatti o la pagina chi siamo).

- ci sono gia' gli `shows` che sono le trasmissioni, a questi `shows` abbiamo aggiunto dei metadati che sono:
    - `is_active` che indica se la trasmissione e' attiva o e' una trasmissione conclusa e di cui pero' teniamo lo storico.
    - `when` che indica quando la trasmissione va in onda. Questo campo da UI e' singolo ma da db e' possibile fare delle magie manualmente per indicare diverse fasce orarie (es. info o bobina...)
    <!-- - `slot_day`, `slot_start`, `slot_end` che indicano rispettivamente il giorno e l'orario in cui la trasmissione andra' in onda (no, una trasmissione non puo' ) -->

- abbiamo poi i `podcast` che dovrebbero essere associati agli `shows` ma questo in wordpress non e' cosi' banale come potrebbe: le associazioni su WP si fanno con le tassonomie, non si possono fare associazioni tra diversi custom `post type` e quindi bisogna arrangiarsi, il vecchio tema usava una tassonomia chiamata `podcastfilter` ma sostanzialmente senza validazione alcuna, quindi e' un campo multiplo che contiene ogni tanto lo slug di uno shows ma spesso cose a caso, quindi bisogna fare pulizia.


<!-- Per non fare confusione creo una nuova tassonomia `show` sui `podcast` usando come slug della tassonomia da associare al podcast proprio lo slug della trasmisisone.
Le operazioni da fare sul db sono: -->
<!-- - per ogni post controllare la tassonomia `podcastfilter` e aggiungere tutti i termini come tag tranne quelli i cui slug combaciano con slug di `show`. -->


#### questione immagini
(problemi di spazio, di copyright, di ottimizzazione)  
https://www.hostinger.com/tutorials/wordpress-images-sizes
https://www.wpbeginner.com/wp-tutorials/how-to-prevent-wordpress-from-generating-image-sizes/

aggiungendo un'immagina a wordpress, questo genera almeno 3 (spesso 5) immagini di diverse grandezze (i temi e i plugin possono anche aggiungere altre dimensioni per le immagini, puoi ritrovarsi anche con 5/10 copie della stessa immagine di dimensioni differenti).

https://wordpress.stackexchange.com/questions/357955/wp-5-3-removing-default-wordpress-image-sizes
https://allthings.how/regenerate-thumbnails-delete-old-ones-wordpress/

uso nuxt/image lato client
default format?
ipx non fa cache locale!


#### nginx cache
occhio al supporto Range per gli audio



#### gestione eventi

#### wp
- [ ] upload non scrivibile?
- [ ] non salva la trasmissione

#### ricerca
- [ ] possibilita' di vedere quanti sono i match per raggruppamento
- [x] nei podcast vorrei avere anche il raggruppamento per trasmissione

#### Componenti
- [x] un componente per il title / header / hero, dai
- [x] il player e' presente in molteplici posti quindi deve mantenere lo stato
- [ ] un componente per i related ()


# Utilities
Servono un po' di utilities per eseguire task specifici:
- [ ] backup
- [ ] restore
- [ ] come ottengo il db che mi serve a partire da quello in prod attuale della radio?
- [ ] migrazioni del db

# cose tech fastidiose
- [ ] default-src per il CSP nel dominio demo

# cose da fare
- [ ] questione alta visibilita' dei post
- [x] search typesense
- [ ] Footer
- [ ] Menù mobile
- [x] Player da sistemare (si ferma se ascolto singolo Podcast)
- [x] Gestione immagini e thumb
- [x] Player pre-footer (ragionarci)

## Home
- [ ] Card Podcast 
- [x] Summary + Tag per prima notizia info
- [ ] Banner Player
- [x] Card Eventi 

## Archivio Info
- [ ] Ultima notizia o notizia in evidenza in banner grigio
- [ ] Sistemare Card con Sommario
- [ ] Tag Cloud (20 più utilizzati)

## Single Info
- [ ] Formattazione pagina
- [ ] 3 Related post

## Palinsesto
- [X] Capire come gestirla (?)
- [X] Definire schema JSON
- [x] Implementare collante tra nuxt e WP
- [ ] Formattazione pagina

## Archivio Trasmissioni
- [ ] Pre Footer link a trasmissioni passate e Archivio Trasmissioni passate
- [ ] Filtri e Categorie

## Single Trasmissioni
- [ ] Formattazione 
- [ ] Ultimi 3 Podcast della trasmissione con link vedi tutti
- [ ] Aggiungere pagina Archivio Podcast per trasmissione

## Archivio Podcast
- [ ] Formattazione
- [ ] Tag
- [ ] Filtri

## Single Podcast
- [ ] Formattazione
- [ ] Tag
- [ ] Related 3 Podcast per TAG

## Archivio Eventi
- [x] prossimi eventi
- [ ] ci sono ma c'e' un problema con gli slug e manca la paginazione, un cerca...

## Single Eventi
- [x] fatto

## About
- [ ] Da fare

## Sostieni

# Bugs
- [ ] immagine fallback
- [ ] search!
- [ ] retrocompatibilita' url! podcastfilter/:show > shows/:show
- [ ] mobile
- [ ] trasmissioni con piu' slot temporali settimanali (puo' essere che all'inizio lo facciamo da db)
- [ ] risistemare le tassonomie (podcastfilter, show, tags)
- [ ] come gestisco il link all'admin (come capisco che e' autenticato?)
- [ ] le podcast card!
- [ ] alcuni title sono da urldecodare? tipo i nomi delle trasmissioni sono encoded nei title ma non nei podcast?
    http://localhost:3000/shows/4x4-monster-track
    http://localhost:3000/podcast/4x4-monster-track-podcast-02
- [ ] pagine con tags troppo lunghi, i tag sforano (e.g. http://localhost:3000/2023/11/gaza-il-piano-inclinato-della-nuova-nakba)