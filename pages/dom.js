
const DOMPurify = require('dompurify')
let domPurify

// const { JSDOM } = require('jsdom')
// const { window } = new JSDOM('<!DOCTYPE html>')
// const domPurify = DOMPurify(window)

// domPurify.addHook('beforeSanitizeElements', node => {
//   if (node.hasAttribute && node.hasAttribute('href')) {
//     const href = node.getAttribute('href')
//     const text = node.textContent

//     // remove FB tracking param
//     if (href.includes('fbclid=')) {
//       try {
//         const url = new URL.URL(href)
//         url.searchParams.delete('fbclid')
//         node.setAttribute('href', url.href)
//         if (text.includes('fbclid=')) {
//           node.textContent = url.href
//         }
//       } catch (e) {
//         return node
//       }
//     }
//   }
//   return node
// })

export function sanitizeHTML(html) {
  if (!domPurify) {
    if (process.client) {
      domPurify = DOMPurify(window)
    } else if (!domPurify) {
      const { JSDOM } = require('jsdom')
      const { window } = new JSDOM('<!DOCTYPE html>')
      domPurify = DOMPurify(window)
    }
  }
  // console.error('HTML PRIMA: ', html)
  const after = domPurify.sanitize(html, {
    ALLOWED_TAGS: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'br', 'i', 'span', 'img',
    'h6', 'b', 'a', 'li', 'ul', 'ol', 'code', 'blockquote', 'u', 's', 'strong'],
    ALLOWED_ATTR: ['href', 'target', 'src']
  })
  // console.error('DOPO: ', after)
  return after
}
