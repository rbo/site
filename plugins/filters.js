import Vue from 'vue'

Vue.filter('capitalize', s => s?.toUpperCase() ?? '')

Vue.filter('timeslot', s => {
  let timeslot = ''
  if (s?.timeslots?.length === 1) {
    timeslot = `ogni ${s.timeslots[0]?.day} dalle ${s?.timeslots[0].start}`
    if (s.timeslots[0]?.end) {
      timeslot += ` alle ${s?.timeslots[0]?.end}`
    }
    return timeslot
  }
})

Vue.filter('postDate', post => new Date(post?.date).toLocaleDateString('it', { weekday: 'long', month: 'long', day: 'numeric', year: 'numeric' }))
Vue.filter('date', date => {
  if (!date) return ''
  return new Date(date).toLocaleString('it', { weekday: 'long', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' } )
})

Vue.filter('dateonly', date => {
  if (!date) return ''
  return new Date(date).toLocaleString('it', { weekday: 'long', month: 'long', day: 'numeric' } )  
})

Vue.filter('link', post => {
  const d = new Date(post?.date)
  return `${d.getFullYear()}/${d.getMonth()}/${post.slug}`
})

Vue.filter('cdnImg', img => {
  if (!img) {
    return ''
  }
  
  img = img.replace("http://rbo.loc/wp-content/uploads/", "").replace('https://rbo.cisti.org/wp-content/uploads/','')
  return `https://cdn.radioblackout.org/wp-content/uploads/${img}`
})

Vue.filter('featuredImg', post => post?.featured_media ? `https://cdn.radioblackout.org/wp-content/uploads/${post?.featured_media}` : '' )