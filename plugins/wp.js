export default ({ app }, inject) => {
  const api = {


    async _gets (type='posts', page=1, per_page=10) {
      const ret = await app.$http.get(`/wp-json/wp/v2/${type}?per_page=${per_page}&page=${page}`)
      const pages = Number(ret.headers.get('x-wp-totalpages'))
      const count = Number(ret.headers.get('x-wp-total'))
      const data = await ret.json()
      return { data, count, pages }
    },

    async getTag (tag_id) {
      return app.$http.$get(`/wp-json/wp/v2/post/${api.pages[page]}`)
    },

    pages: {
      'chi-siamo': 82,
      'contatti': 183
    },
    async getPage (page) {
      return app.$http.$get(`/wp-json/wp/v2/pages/${api.pages[page]}`)
    },

    async getHotPosts (page=1, per_page=10) {
      const ret = await app.$http.get(`/wp-json/wp/v2/posts?per_page=${per_page}&page=${page}&categories=10623`)
      const pages = Number(ret.headers.get('x-wp-totalpages'))
      const posts = await ret.json()
      return { posts, pages }
    },

    async getTag (tag_slug) {
      return app.$http.$get(`/wp-json/wp/v2/tags/?slug=${tag_slug}`)
    },

    async getPosts (page=1, per_page=12) {
      const ret = await app.$http.get(`/wp-json/wp/v2/posts?per_page=${per_page}&page=${page}&categories_exclude=10623`)
      const pages = Number(ret.headers.get('x-wp-totalpages'))
      const posts = await ret.json()
      return { posts, pages }
    },

    getPost (slug) {
      return app.$http.$get('/wp-json/wp/v2/posts/?slug=' + slug )
    },

    async getEvents (older = false) {
      // cache is needed
      if (older) {
        const ret = await app.$http.get(`https://gancio.cisti.org/api/collections/Blackout?older=true&max=12&reverse=true`)
        return ret.json()
      } else {
        const ret = await app.$http.get(`https://gancio.cisti.org/api/collections/Blackout`)
        return ret.json()
      }
    },

    async getEvent (slug) {
      const ret = await app.$http.get(`https://gancio.cisti.org/api/event/detail/${slug}`)
      return ret.json()
    },

    getPodcasts (page=1, per_page = 9) {
      return api._gets('podcast', page, per_page)
    },

    getPodcast (slug) {
      return app.$http.$get('/wp-json/wp/v2/podcast/?slug=' + slug )
    },

    /**
     *
     * @param {String} show_slug
     * @param {Number} page
     * @returns podcast related to specified show
     */
    async getPodcastsByShow (show_slug, page=1) {
      const ret = await app.$http.get(`/wp-json/wp/v2/podcast?per_page=12&page=${page}&show=${show_slug}`)
      const pages = Number(ret.headers.get('x-wp-totalpages'))
      const podcasts = await ret.json()
      return { podcasts, pages }
    },

    async getPostsByTag (tags, page=1, per_page=6) {
      const ret = await app.$http.get(`/wp-json/wp/v2/posts?per_page=${per_page}&page=${page}&tags=${tags}`)
      const total_pages = Number(ret.headers.get('x-wp-totalpages'))
      const posts = await ret.json()
      return { posts, total_pages }
    },

    async getPodcastsByTag (tags, page=1, per_page=6) {
      const ret = await app.$http.get(`/wp-json/wp/v2/podcast?per_page=${per_page}&page=${page}&tags=${tags}`)
      const total_pages = Number(ret.headers.get('x-wp-totalpages'))
      const podcasts = await ret.json()
      return { podcasts, total_pages }
    },

    async getShows () {
      return app.$http.$get(`/wp-json/wp/v2/shows?per_page=100`)
    },

    async getShow (show) {
      const ret = await app.$http.$get(`/wp-json/wp/v2/shows?slug=${show}`)
      return ret[0]
    },

    getPalinsesto () {
      return app.$http.$get(`/wp-json/rbo/v1/palinsesto`)
    }

  }

  inject('wp', api)
}
