// import { createIPX, createIPXMiddleware, createIPXPlainServer, createIPXWebServer } from 'ipx'

// // https://github.com/unjs/ipx
// const ipx = createIPX({
//   dir: '', // absolute path to images dir
//   domains: ['https://cdn.radioblackout.org'],
//   alias: {}, // base alias
//   sharp: {}, // sharp options
// })

// export default createIPXMiddleware(ipx)


import {
  createIPX,
  createIPXNodeServer,
} from "ipx";

import path from "path";

import { ofetch } from "ofetch";
import { createError } from "h3";

import { createHash } from "crypto";
import { createWriteStream, existsSync } from "fs";

import { appendFile, readFile, writeFile } from "fs/promises";

const HTTP_RE = /^https?:\/\//;

export function ipxHttpStorage(_options = {}) {
  const allowAllDomains = _options.allowAllDomains ?? false;
  let _domains = _options.domains || [];
  const defaultMaxAge = _options.maxAge || 300;
  const fetchOptions = _options.fetchOptions || {};

  if (typeof _domains === "string") {
    _domains = _domains.split(",").map((s) => s.trim());
  }

  const domains = new Set(
    _domains
      .map((d) => {
        if (!HTTP_RE.test(d)) {
          d = "http://" + d;
        }
        return new URL(d).hostname;
      })
      .filter(Boolean),
  );

  // eslint-disable-next-line unicorn/consistent-function-scoping
  function validateId(id, opts) {
    const url = new URL(decodeURIComponent(id));
    if (!url.hostname) {
      throw createError({
        statusCode: 403,
        statusText: `IPX_MISSING_HOSTNAME`,
        message: `Hostname is missing: ${id}`,
      });
    }
    if (!allowAllDomains && !domains.has(url.hostname)) {
      throw createError({
        statusCode: 403,
        statusText: `IPX_FORBIDDEN_HOST`,
        message: `Forbidden host: ${url.hostname}`,
      });
    }
    return url.toString();
  }

  // eslint-disable-next-line unicorn/consistent-function-scoping
  function parseResponse(response) {
    let maxAge = defaultMaxAge;
    if (_options.ignoreCacheControl !== true) {
      const _cacheControl = response.headers.get("cache-control");
      if (_cacheControl) {
        const m = _cacheControl.match(/max-age=(\d+)/);
        if (m && m[1]) {
          maxAge = Number.parseInt(m[1]);
        }
      }
    }

    let mtime;
    const _lastModified = response.headers.get("last-modified");
    if (_lastModified) {
      mtime = new Date(_lastModified);
    }

    return { maxAge, mtime };
  }

  return {
    name: "ipx:http",
    async getMeta(id, opts) {
      const url = validateId(id);
      try {
        const response = await ofetch.raw(url, {
          ...fetchOptions,
          method: "HEAD",
        });
        const { maxAge, mtime } = parseResponse(response);
        return { mtime, maxAge };
      } catch {
        return {};
      }
    },

    async getData(id, opts) {
      const url = validateId(id);
      const fileName = path.resolve('./downloads', createHash('md5').update(id).digest('hex'))

      if (existsSync(fileName)) {
        return readFile(fileName)
      }

      const response = await ofetch(url, {
        ...fetchOptions,
        method: "GET",
        responseType: "arrayBuffer",
      });

      await writeFile(fileName, Buffer.from(response));

      return response
    },
  };
}


const ipx = createIPX({
  httpStorage: ipxHttpStorage({ domains: ['https://cdn.radioblackout.org', 'https://gancio.cisti.org', 'https://radioblackout.org', 'http://radioblackout.org'] }),
});


export default createIPXNodeServer(ipx);
