export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  
  head: {
    title: "Radio Blackout",
    htmlAttrs: {
      lang: "it"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.png" }],
    //link: [{ rel: "stylesheet", href: "/tailwind.css" }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["bulma/css/bulma.min.css", './assets/custom.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ["~/plugins/wp.js", "~plugins/filters.js", "~/plugins/intersect.js"],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "@nuxtjs/tailwindcss",
    ['@nuxtjs/fontawesome', { component: 'fa', suffix: true, icons: {
      solid: ['faSearch', 'faClose', 'faSquareCheck', 'faSquareFull', 'faChevronLeft', 'faChevronRight', 'faForwardStep', 'faBackwardStep' ]
    } } ]
  ],

  serverMiddleware: [
    { path: '/ipx', handler: '~/server-middleware/ipx.js' },
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxt/http", "@nuxtjs/tailwindcss"],

  http: {
    proxy: true
  },

  render: {
    csp: {
      reportOnly: true,
      policies: {
        'img-src': ["'self'", 'https://cdn.radioblackout.org', 'https://radioblackout.org', 'http://radioblackout.org'],
      }
    }
  },
  proxy: {
    "/wp-content": "https://cdn.radioblackout.org",
	  "/wp-json/": "http://wp",
    "/multi_search": "http://typesense:8108/",

  },

  server: {
    host: '0.0.0.0',
    port: 8000
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
};
