# makefile

# use the rest as arguments for "run"
RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
# ...and turn them into do-nothing targets
$(eval $(RUN_ARGS):;@:)


dev:
	docker compose up

start:
	docker compose -f docker-compose.production.yml up --build

log:
	docker compose logs -f -n 10

backup:
	docker run --rm \
		--mount source=rbo-next_db-data,target=/var/lib/mysql \
		-v ./:/backup \
		busybox \
		tar -czvf /backup/rbo_`date +%F`.tar.gz /var/lib/mysql

dump:
	docker compose exec db mariadb-dump rbo -uroot -prbo | gzip > rbo_`date +%F`.dump.gz

import_db:
	cat $(RUN_ARGS) | docker compose exec -T db mariadb -u root -prbo rbo

db:
	docker compose exec db mysql rbo -uroot -prbo

### immagine docker di wordpress cli agganciata al db e al network di produzione
## esempi:
## lista degli utenti: `make cli user list`
## 
cli:
	docker run -it --rm \
		--volumes-from rbo-next-wp-1 \
		--network rbo-next_default \
		-e WORDPRESS_DB_HOST=db \
		-e WORDPRESS_DB_USER=rbo \
		-e WORDPRESS_DB_PASSWORD=rbo \
		-e WORDPRESS_DB_NAME=rbo \
		-e WORDPRESS_TABLE_PREFIX=wp_blackout_ \
		wordpress:cli $(RUN_ARGS)