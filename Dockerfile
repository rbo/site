FROM node:20-alpine AS build
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --pure-lockfile
COPY . .
RUN yarn build



FROM node:20-alpine
WORKDIR /app
COPY ./package.json /app/
COPY ./nuxt.config.js /app/nuxt.config.js
COPY ./server-middleware /app/server-middleware
COPY ./static /app/static
COPY --from=build /app/.nuxt /app/.nuxt
COPY --from=build /app/node_modules /app/node_modules
RUN mkdir /app/downloads
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=8000
EXPOSE 3000
ENTRYPOINT [ "yarn", "start" ]
# CMD ["nginx", "-g", "daemon off;"]
